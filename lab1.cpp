#include <iostream>


class Node
{
public:
	float a[3];

	Node* next = nullptr;

	void print()
	{
		for (int i = 0; i < 3; i++)
		{
			std::cout << a[i] << " ";
		}
		std::cout << std::endl;
	}

	float calculate_sum()
	{
		return a[0] + a[1] + a[2];
	}

	void fill(float arr[3])
	{
		for (int i = 0; i < 3; i++)
		{
			a[i] = arr[i];
		}
	}
};

class List
{
public:
	void print()
	{
		Node* node = head;
		while (node)
		{
			node->print();
			node = node->next;
		}
	}

	void add(float arr[3])
	{
		if (!head)
		{
			head = new Node;
			head->fill(arr);
			last = head;
			return;
		}
		last->next = new Node;
		last->next->fill(arr);
		last = last->next;
	}

	float calculate_average()
	{
		float sum = 0.0;
		int count = 0;
		Node* node = head;
		while (node)
		{
			count += 3;
			sum += node->calculate_sum();
			node = node->next;
		}
		return sum / count;
	}

private:
	Node* head = nullptr;
	Node* last = nullptr;

};

int main()
{
	float arr[3][3] = { { 3, 2, 2 }, { 2, 2, 9 }, { 6, 5, 2} };
	List list;

	list.add(arr[0]);
	list.add(arr[1]);
	list.add(arr[2]);
	std::cout << "List on screen:"<< std::endl;
	list.print();
	
	std::cout << "Average in list: " << list.calculate_average() << std::endl;

	return 0;
}