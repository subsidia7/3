#include <iostream>


class Node
{
public:
	Node* next = nullptr;

	void print()
	{
		for (int i = 0; i < 3; i++)
		{
			std::cout << a[i] << " ";
		}
		std::cout << std::endl;
	}

	float calculate_sum()
	{
		return a[0] + a[1] + a[2];
	}

	void fill(float arr[3])
	{
		for (int i = 0; i < 3; i++)
		{
			a[i] = arr[i];
		}
	}

	float get_min()
	{
		float min = a[0];
		for (int i = 1; i < 3; i++)
		{
			if (a[i] < min)
			{
				min = a[i];
			}
		}
		return min;
	}

	float* get()
	{
		return a;
	}

	~Node()
	{
	}

private:
	float a[3];
};

class List
{
public:

	List()
	{
	}

	List(float matrix[][3], int size)
	{
		for (int i = 0; i < size; i++)
		{
			this->add(matrix[i]);
		}
	}
	
	// Copy construcor
	List(const List &right)
	{
		Node* right_node = right.head;
		while (right_node)
		{
			this->add(right_node->get());
			right_node = right_node->next;
		}
	}

	void print()
	{
		Node* node = head;
		while (node)
		{
			node->print();
			node = node->next;
		}
	}

	void add(float arr[3])
	{
		if (!head)
		{
			head = new Node;
			head->fill(arr);
			last = head;
			return;
		}
		last->next = new Node;
		last->next->fill(arr);
		last = last->next;
	}

	float calculate_average()
	{
		float sum = 0.0;
		int count = 0;
		Node* node = head;
		while (node)
		{
			count += 3;
			sum += node->calculate_sum();
			node = node->next;
		}
		return sum / count;
	}
	
	friend float get_min(List &);

	~List()
	{
		Node* temp = head;
		while (head)
		{
			head = head->next;
			delete temp;
			temp = head;
		}
	}

private:
	Node* head = nullptr;
	Node* last = nullptr;
};


float get_min(List &list)
{
	float min;
	if (list.head)
	{
		min = list.head->get_min();
	}
	Node* node = list.head->next;
	float next_min;
	while (node)
	{
		next_min = node->get_min();
		if (min < next_min)
		{
			min = next_min;
		}
		node = node->next;
	}
	return min;
}

int main()
{
	float arr[4][3] = { { 3, 2, 2 }, { 2, 2, 9 }, { 6, 5, 2}, { 10, 20, 30} };

	List list(arr, 4);
	std::cout << "List on screen:" << std::endl;
	list.print();
	std::cout << "Average in list: " << list.calculate_average() << std::endl;
	std::cout << "Min in list: " << get_min(list) << std::endl << std::endl;

	List copied_list = list;
	std::cout << "Copied_list on screen:" << std::endl;
	copied_list.print();
	std::cout << "Average in copied_list: " << copied_list.calculate_average() << std::endl;
	std::cout << "Min in copied_list: " << get_min(copied_list) << std::endl;
	
	return 0;
}